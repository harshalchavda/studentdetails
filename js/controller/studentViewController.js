studentApp.controller('studentViewController', ['$scope', '$location', 'studentFactory', function ($scope, $location, studentFactory) {
	$scope.studentList;

	studentFactory.listStudentDetail().then(function (data) {
		$scope.studentList = data;
	});

	$scope.deleteStudent = function (id) {
		studentFactory.deleteStudentDetail(id);
		$location.url('/viewStudents');
	}
}]);