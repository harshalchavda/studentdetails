studentApp.controller('studentAddController', ['$scope', 'studentFactory', '$location', '$routeParams', function ($scope, studentFactory, $location, $routeParams) {


	$scope.newStudent = {};

	$scope.addStudent = function () {
		studentFactory.saveStudentDetail($scope.newStudent);
		$scope.newStudent = {};
		$location.url('/viewStudents');
	}

	if (!(typeof $routeParams.id === 'undefined')) {
		$scope.newStudent = studentFactory.getStudentDetail($routeParams.id);
		$scope.showUpdate = true;
	}

}]);