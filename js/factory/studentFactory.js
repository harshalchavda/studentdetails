studentApp.factory('studentFactory', function ($http) {
    var studentFactoryObj = {};

    var uid = 2;

    var students;

    studentFactoryObj.saveStudentDetail = function (student) {
        if (student.id == null) {
            student.id = uid++;
            students.push(student);
        } else {
            for (i in students) {
                if (students[i].id == student.id) {
                    students[i] = student;
                }
            }
        }
    }

    studentFactoryObj.getStudentDetail = function (id) {
        for (i in students) {
            if (students[i].id == id) {
                return angular.copy(students[i]);
            }
        }

    }

    studentFactoryObj.deleteStudentDetail = function (id) {
        for (i in students) {
            if (students[i].id == id) {
                students.splice(i, 1);
            }
        }
    }

    studentFactoryObj.listStudentDetail = function () {
        var promise = $http.get('./data.json').then(function (res) {
            students = students || res.data.list;
            return students;
        });
        return promise;
    }

    return studentFactoryObj;
});