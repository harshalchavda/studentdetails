studentApp.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/viewStudents', {
		templateUrl: 'views/viewStudents.htm',
		controller: 'studentViewController'
	})
	.when('/addStudent', {
		templateUrl: 'views/addStudent.htm',
		controller: 'studentAddController'
	})
	.when('/addStudent/:id', {
		templateUrl: 'views/addStudent.htm',
		controller: 'studentAddController'
	})
	.otherwise({ redirectTo: '/viewStudents' });
}]);